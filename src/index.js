import dva from 'dva';
import './index.css';
import createLoading from 'dva-loading';
// 1. Initialize
import { browserHistory } from 'dva/router';
const app = dva({
  history: browserHistory,
});

app.model(require("./models/users"));
app.model(require("./models/explorer"));
app.model(require("./models/courses"));
app.model(require("./models/lesson"));
app.model(require("./models/mine"));
// 2. Plugins
// app.use({});
app.use(createLoading());
// 3. Model
// app.model(require('./models/example'));

// 4. Router
app.router(require('./router'));

// 5. Start
app.start('#root');

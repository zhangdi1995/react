/**
 * Created by xzsmstephen on 2017/2/20.
 */
import requestNoRecords from '../utils/requestNoRecords'
export function fetchMessage(userPhone){
  console.log(userPhone)
  let paramas={"sms" : {"phone" : userPhone}};
  return requestNoRecords(`/api/post/sms/request`, {
    method: "POST",
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(paramas)
  })
}
export function bindPhone({payload}){
  let userId =payload.userId;
  let paramas={"sms" : {"phone" : payload.userPhone,"sms_code":payload.smsCode,"uid" : userId}};
  return requestNoRecords(`/api/post/sms/validate`, {
    method: "POST",
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(paramas)
  })
}
export function postLoginForm({payload}){
  let paramas={"sms" : {"phone" : payload.userPhone,"sms_code":payload.smsCode}};
  return requestNoRecords(`/api/post/sms/validate`, {
    method: "POST",
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(paramas)
  })
}
export function postWechatCode({payload}){
  let paramas={"code":payload.code};
  return requestNoRecords(`/api/wechat/login`, {
    method: "POST",
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(paramas)
  })
}
export function refreshAccessToken({payload}){
  let paramas={ "token" : { "refresh_token" : payload.refresh_token } }
  return requestNoRecords(`/api/users/refresh`, {
    method: "POST",
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(paramas)
  })
}
export function updateUserInfo({payload,token}){
  if(payload.hasOwnProperty('nick')) {
    var paramas = {"user": {"nick": payload.nick}};
  }
  else if(payload.hasOwnProperty('email')) {
    var paramas = {"user": {"email": payload.email}};
  }
  else{
    var paramas = {"user": {}};
  }
  let userId =payload.userId;
  return requestNoRecords(`/api/update/users/${userId}`, {
    method: "POST",
    headers: {
      'content-type': 'application/json',
      "Authorization":"Bearer "+token.access_token
    },
    body: JSON.stringify(paramas)
  })
}
export function getUserInfo({payload,token}){
  let userId =payload.userId;
  return requestNoRecords(`/api/get/users/${userId}`, {
    method: "GET",
    headers: {
      'content-type': 'application/json',
      "Authorization":"Bearer "+token.access_token
    }
  })
}
/*- 请求上传头像
	- /users/[id]/request_avatar_upload
		- Header
		    - Authorization: Bearer ACCESS_TOKEN
		- 发送字段
		    - 无
		- 返回字段
		    - {"res_name" : "Qiniu resource name", "uptoken" : "Qiniu upload token"}
		- 说明
		    - 先调用此接口，获取资源名和uptoken之后，使用七牛的客户端SDK(iOS, JS等)上传图片*/
export function getUploadToken({payload,token}){
  let userId =payload.userId;
  return requestNoRecords(`/api/users/${userId}/request_avatar_upload`, {
    method: "POST",
    headers: {
      "Authorization":"Bearer "+token.access_token
    }
  })
}


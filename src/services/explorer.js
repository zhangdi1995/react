/**
 * Created by xzsmstephen on 2017/2/17.
 */
import request from '../utils/request'

export function fetch({page,token}){
  return request(`/api/get/projects`,{headers:{"Authorization":"Bearer "+token.access_token}})
}


/**
 * Created by xzsmstephen on 2017/2/18.
 */
import request from '../utils/request'
import requestNoRecords from '../utils/requestNoRecords'

export function fetch({lessonId,token}){
  console.log('zhengzailesson')
  return requestNoRecords(`/api/get/lessons/${lessonId}`,{headers:{"Authorization":"Bearer "+token.access_token}})
}
export function fetchComments({lessonId,token}){
  console.log('zhengzailesson')
  return requestNoRecords(`/api/get/lessons/${lessonId}/comments`,{headers:{"Authorization":"Bearer "+token.access_token}})
}
export function fetchSignature({vid,token}){
  return requestNoRecords(`/api/video/sign?vid=plv_${vid}`,{headers:{"Authorization":"Bearer "+token.access_token}})
}

/**
 * Created by xzsmstephen on 2017/2/16.
 */
import request from '../utils/request'
import {PAGE_SIZE} from '../constants'
export function fetch({page}){
  return request(`/api/users`)
}
export function remove(id) {
  return request(`/api/users/${id}`, {
    method: 'DELETE',
  });
}
export function patch(id, values) {
  return request(`/api/users/${id}`, {
    method: 'PATCH',
    body: JSON.stringify(values),
  });
}

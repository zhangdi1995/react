/**
 * Created by xzsmstephen on 2017/2/18.
 */
import request from '../utils/request'
import requestNoRecords from '../utils/requestNoRecords'
export function fetch({page,token}){
  return request(`/api/get/courses`,{headers:{"Authorization":"Bearer "+token.access_token}})
}
export function fetchComments({courseId,token}){
  console.log('zhengzaicommen')


  return requestNoRecords(`/api/get/courses/${courseId}/comments`,{headers:{"Authorization":"Bearer "+token.access_token}})
}
export function fetchLesson({lessonId,token}){
  console.log('zhengzailesson')
  return requestNoRecords(`/api/get/lessons/${lessonId}`,{headers:{"Authorization":"Bearer "+token.access_token}})
}
export function fetchSignature({vid,token}){
  return requestNoRecords(`/api/video/sign?vid=${vid}`,{headers:{"Authorization":"Bearer "+token.access_token}})
}

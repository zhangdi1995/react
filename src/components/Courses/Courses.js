import React from 'react';
import { connect } from 'dva';
import {Link} from 'dva/router'
import style from './Courses.css'
import {Toast,ActivityIndicator,Tabs,WhiteSpace,Accordion,List} from 'antd-mobile'
import PolyPlayer from './PolyPlayer'
const TabPane=Tabs.TabPane;
function Courses({ dispatch,list:dataSource,currentCourse,loading,comments,signature}) {
  if(loading){
    return (

      <ActivityIndicator size="large" className={style.loading_position} />
      )

  }
  else {
    return (
      <div >
        <PolyPlayer vid={currentCourse.video.url} signature={signature}/>
        <Tabs defaultActiveKey="1" >
          <TabPane tab="课程大纲" key="1">
            <Accordion defaultActiveKey="0" className="my-accordion">
              {currentCourse.chapters.map(data=>(
                <Accordion.Panel header={data.name}>
                  <List className="my-list">
                    {data.public_lessons.map(lesson=>(
                      <List.Item><Link to={`/lessons/${lesson.id}`}><p className={style.link_text}>{lesson.name}</p></Link></List.Item>
                    ))}
                  </List>
                </Accordion.Panel>
              ))}
            </Accordion>

          </TabPane>
          <TabPane tab="课程讨论" key="2">

              {comments.map(comment=>(
                <div>
                  <img src={comment.poster.avatar_url} width="100px" height="100px"/>
                  <h1 style={{'display':'inline'}}>{comment.poster.nick}</h1>
                  <p>{comment.text}</p>
                </div>
              )
              )}

          </TabPane>
          <TabPane tab="课程简介" key="3">
            <div dangerouslySetInnerHTML={{__html:currentCourse.desc}}/>

          </TabPane>
        </Tabs>

      </div>
    );
  }

}

function mapStateToProps(state) {
  const { list,comments,currentCourse,signature } = state.courses;
  return {
    loading:state.loading.models.courses,
    list,
    comments,
    currentCourse,
    signature
  };
}

export default connect(mapStateToProps)(Courses);

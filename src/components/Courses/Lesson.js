/**
 * Created by xzsmstephen on 2017/2/18.
 */
import React from 'react';
import { connect } from 'dva';
import {Link} from 'dva/router'
import style from './Courses.css'
import {Toast,ActivityIndicator,Tabs,WhiteSpace,Accordion,List} from 'antd-mobile'
import PolyPlayer from './PolyPlayer'
const TabPane=Tabs.TabPane;
function Lesson({ dispatch,lesson:dataSource,loading,comments,signature}) {
  if(loading){
    return (

      <ActivityIndicator size="large" className={style.loading_position} />
      )

  }
  else {
    return (
      <div >
        <PolyPlayer vid={dataSource.video.url} signature={signature}/>
        <Tabs defaultActiveKey="1" >
          <TabPane tab="本节大纲" key="1">
            <div dangerouslySetInnerHTML={{__html:dataSource.desc}}/>
          </TabPane>
          <TabPane tab="随堂练习" key="2">



          </TabPane>
          <TabPane tab="本节讨论" key="3">
          {comments.map(comment=>(
                <div>
                  <img src={comment.poster.avatar_url} width="100px" height="100px"/>
                  <h1 style={{'display':'inline'}}>{comment.poster.nick}</h1>
                  <p>{comment.text}</p>
                </div>
              )
              )}

          </TabPane>
        </Tabs>

      </div>
    );
  }

}

function mapStateToProps(state) {
  const { lesson,comments,signature} = state.lesson;
  return {
    loading:state.loading.models.lesson,
    lesson,
    comments,
    signature
  };
}

export default connect(mapStateToProps)(Lesson);

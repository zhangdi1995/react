/**
 * Created by xzsmstephen on 2017/2/18.
 */
import React ,{Component} from 'react';
import ReactDOM from 'react-dom'
import { connect } from 'dva';
import {Link} from 'dva/router'
import style from './Courses.css'
import {Toast,ActivityIndicator,Tabs,WhiteSpace,Accordion,List} from 'antd-mobile'

export default class PolyPlayer extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let player = polyvObject('#plv_' + this.props.vid).videoPlayer({
      'width': '100%',
      'height': '700',
      'vid': this.props.vid,
      'ts': this.props.signature.ts,
      'sign': this.props.signature.sign

    })
  }

  componentWillReceiveProps(nP) {
    //保利威视的移动端视频插入有问题,当更新<div>的id的时候,原来已经插入的视频不会被删除.所以用下面这句代码删除原来插入的视频节点.
    if(document.getElementById('container' + this.props.vid)){
      this.refs.video.removeChild(document.getElementById('container' + this.props.vid));
      console.log('container' + this.props.vid)
    }
    else{

    }

  }

  componentDidUpdate() {
    polyvObject('#plv_' + this.props.vid).videoPlayer({
      'width': '100%',
      'height': '700',
      'vid': this.props.vid,
      'ts': this.props.signature.ts,
      'sign': this.props.signature.sign
    })
  }

  render() {

    return (
      <div ref='video' id={'plv_'+this.props.vid}/>

    )
  }
}

/**
 * Created by xzsmstephen on 2017/2/17.
 */
import React from 'react';
import { connect } from 'dva';
import {Link} from 'dva/router'
import style from './ContentList.css'
function ContentList({ dispatch,list:dataSource,location}) {
  //function deleteHandler(id) {
  //  dispatch({
  //    type: 'users/remove',
  //    payload: id,
  //  });
  //}
  //function pageChangeHandler(page) {
  //  dispatch(routerRedux.push({
  //    pathname: '/users',
  //    query: { page },
  //  }))
  //}
  //function editHandler(id, values) {
  //  dispatch({
  //    type: 'users/patch',
  //    payload: { id, values },
  //  })
  //}
  //const columns = [
  //  {
  //    title: 'Name',
  //    dataIndex: 'nick',
  //    key: 'nick',
  //    render: text => <a href="">{text}</a>,
  //  },
  //  {
  //    title: 'Email',
  //    dataIndex: 'email',
  //    key: 'email',
  //  },
  //  {
  //    title: 'Website',
  //    dataIndex: 'website',
  //    key: 'website',
  //  },
  //  {
  //    title: 'Operation',
  //    key: 'operation',
  //    render: (text, record ) => (
  //      <span className={styles.operation}>
  //        <UserModal record={record} onOk={editHandler.bind(null, record.id)}>
  //          <a>Edit</a>
  //        </UserModal>
  //        <Popconfirm title="Confirm to delete?" onConfirm={deleteHandler.bind(null, record.id)}>
  //          <a href="">Delete</a>
  //        </Popconfirm>
  //      </span>
  //    ),
  //  },
  //];

  return (
    <div >
      {dataSource.map(data=> {
        if (data.id == location.pathname.slice(10)) {
          return data.sections[0].public_contents.map(content=>
          <Link to={`/contents/${content.id}`}>
            <div className={style.img_box}>
              <img width="100%" src={content.banner}/>
              <p className={style.img_title}>{content.brief}</p>
            </div>
          </Link>)
        }

      })}
    </div>
  );
}

function mapStateToProps(state) {
  const { list } = state.explorer;
  return {

    list
  };
}

export default connect(mapStateToProps)(ContentList);

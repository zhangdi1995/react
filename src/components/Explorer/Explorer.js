import React from 'react';
import { connect } from 'dva';
import {Link} from 'dva/router'
import style from './Explorer.css'
import {Toast,ActivityIndicator} from 'antd-mobile'
function Explorer({ dispatch,list:dataSource,loading}) {
  //function deleteHandler(id) {
  //  dispatch({
  //    type: 'users/remove',
  //    payload: id,
  //  });
  //}
  //function pageChangeHandler(page) {
  //  dispatch(routerRedux.push({
  //    pathname: '/users',
  //    query: { page },
  //  }))
  //}
  //function editHandler(id, values) {
  //  dispatch({
  //    type: 'users/patch',
  //    payload: { id, values },
  //  })
  //}
  //const columns = [
  //  {
  //    title: 'Name',
  //    dataIndex: 'nick',
  //    key: 'nick',
  //    render: text => <a href="">{text}</a>,
  //  },
  //  {
  //    title: 'Email',
  //    dataIndex: 'email',
  //    key: 'email',
  //  },
  //  {
  //    title: 'Website',
  //    dataIndex: 'website',
  //    key: 'website',
  //  },
  //  {
  //    title: 'Operation',
  //    key: 'operation',
  //    render: (text, record ) => (
  //      <span className={styles.operation}>
  //        <UserModal record={record} onOk={editHandler.bind(null, record.id)}>
  //          <a>Edit</a>
  //        </UserModal>
  //        <Popconfirm title="Confirm to delete?" onConfirm={deleteHandler.bind(null, record.id)}>
  //          <a href="">Delete</a>
  //        </Popconfirm>
  //      </span>
  //    ),
  //  },
  //];
  if(loading){
    return (

      <ActivityIndicator size="large" className={style.loading_position} />
      )

  }
  else {
    return (
      <div >
        {dataSource.map(data=>(
          <Link to={`/explorer/${data.id}`}>
            <div className={style.img_box}>
              <img width='100%' src={data.banner}/>
              <p className={style.img_title}>{data.brief}</p>
            </div>
          </Link>
        ))}
      </div>
    );
  }

}

function mapStateToProps(state) {
  const { list } = state.explorer;
  return {
    loading:state.loading.models.explorer,
    list
  };
}

export default connect(mapStateToProps)(Explorer);

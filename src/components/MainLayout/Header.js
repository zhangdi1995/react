/**
 * Created by xzsmstephen on 2017/2/18.
 */
import React from 'react'
import {NavBar,TabBar,Popup,List,Icon,Button,Picker} from 'antd-mobile'
import {hashHistory,Link} from 'dva/router'
import styles from './MainLayout.css'
import {connect} from 'dva'
function Header({location,children,history,list,dispatch}){
  const PickerChildren = (props) => (
    <div onClick={props.onClick}>
        <Icon type="search"/> {props.children}
    </div>
);
  function changeCurrentCourse(e){
    let currentCourse=list.find((value,index)=>value.id==e[0])
    dispatch({type:'courses/changeCurrentCourse',payload:currentCourse})
  }
  switch(location.pathname){
    case '/courses':
          return (
            <NavBar rightContent={
              [<Picker data={list.map(data=>(
              {label:data.name,value:data.id}
              )
              )}
              onChange={changeCurrentCourse}
              title={'选择课类'}
              cols={1} >
                <PickerChildren>选课</PickerChildren>
              </Picker>]
              }
              iconName={null}
              className={styles.fix_top}>
              仟问学堂
            </NavBar>
          )
    case '/explorer':
          return (
            <NavBar  iconName={null}  className={styles.fix_top}>
              仟问学堂
            </NavBar>
          )
    default:
          return <NavBar >仟问学堂</NavBar>
  }

}
function mapStateToProps(state){
  const {list}=state.courses;
  return {
    list
  }
}
export default connect(mapStateToProps)(Header);

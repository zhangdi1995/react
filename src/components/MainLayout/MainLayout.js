/**
 * Created by xzsmstephen on 2017/2/17.
 */
import React from 'react'
import {NavBar,TabBar,WhiteSpace} from 'antd-mobile'
import styles from  './MainLayout.css'
import {hashHistory,Link} from 'dva/router'
import ExplorerComponent from '../Explorer/Explorer'
import Header from './Header'
export default function MainLayout({children,location,history}){
  function iconByLocation(){
    switch(location.pathname){
      case '/courses':
            return '选课';
      case '/explorer':
            return 'left'
    }

  }

  return(
    <div className={styles.back_white}>
      <Header location={location} history={history}/>
      <div >
        <WhiteSpace/>
        <WhiteSpace/>
        <WhiteSpace/>
        <WhiteSpace/>
        <WhiteSpace/>
        {children}
        <WhiteSpace/>
        <WhiteSpace/>
        <WhiteSpace/>
        <WhiteSpace/>
        <WhiteSpace/>
      </div>
      {location.pathname=='/'?'':
        <TabBar>
        <TabBar.Item title="课程"  onPress={e=>{return location.pathname=='/courses'?'':history.push('/courses')}} key={1}
                     selected={location.pathname=='/courses'?true:false} selectedIcon={{uri: 'https://zos.alipayobjects.com/rmsportal/UNQhIatjpNZHjVf.png'}}
                     icon={{ uri: 'https://zos.alipayobjects.com/rmsportal/UNQhIatjpNZHjVf.png' }}>

        </TabBar.Item>
        <TabBar.Item title="探索"  onPress={e=>{return location.pathname=='/explorer'?'':history.push('/explorer')}} key={2}
                     selected={location.pathname=='/explorer'?true:false} selectedIcon={{uri: 'https://zos.alipayobjects.com/rmsportal/UNQhIatjpNZHjVf.png'}}
                     icon={{ uri: 'https://zos.alipayobjects.com/rmsportal/UNQhIatjpNZHjVf.png' }}>

        </TabBar.Item>
        <TabBar.Item title="我的仟问"  onPress={e=>{return location.pathname=='/mine'?'':history.push('/mine')}} key={3}
                     selected={location.pathname=='/mine'?true:false} selectedIcon={{uri: 'https://zos.alipayobjects.com/rmsportal/UNQhIatjpNZHjVf.png'}}
                     icon={{ uri: 'https://zos.alipayobjects.com/rmsportal/UNQhIatjpNZHjVf.png' }}>

        </TabBar.Item>

      </TabBar>}

    </div>
  )
}

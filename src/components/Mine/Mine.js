import React from 'react';
import {List,Button,Picker,Modal,ImagePicker} from 'antd-mobile'
import styles from './Mine.css'
import {connect} from 'dva'
import {Link} from 'dva/router'
import {createForm} from 'rc-form'
import {district,grade} from '../../constants'
import Bind from './Binding'
const alert=Modal.alert;
const prompt=Modal.prompt;
function Mine({dispatch,userInfo,form,uploadToken,history}) {
  function clickImgToUpload(){
    document.getElementById('file').click()
  }
  function  upload(uploadToken){

    console.log(uploadToken,'uptoken1');
    let formData = new FormData();
    formData.append('token',uploadToken.uptoken);
    formData.append('key',uploadToken.res_name);
    formData.append('file',document.getElementById('file').files[0]);
    $.ajax({
      url:'http://upload.qiniu.com/',
      type:'POST',
      data:formData,
      processData:false,
      contentType:false,
      success:data=>dispatch({type:'mine/getUserInfo',payload:{userId:user.id}}),
      error:err=>console.log(err)
    }
    )
  }
  function logout() {
    dispatch({type: 'mine/logout'});
    history.push('/');
    alert('成功登出')

  }

  console.log(uploadToken,'uptoken2')
  let user=userInfo.user;
  const {getFieldProps} = form;
  console.log(district);
  return (

    <div className={styles.normal}>
      <div className={styles.user_img_box}>
        <div className={styles.user_img}>
          <input name="file" onChange={()=>dispatch({type:'mine/getUploadToken',payload:{userId:user.id,callback:upload}})} id='file' type="file" style={{'display':'none'}} />
          <img onClick={clickImgToUpload} src={user.avatar_url} />
        </div>
      </div>
      <List>
        <List.Item  thumb="https://zos.alipayobjects.com/rmsportal/dNuvNrtqUztHCwM.png"
                    onClick=
                      {() => prompt('昵称', '', [
            { text: '取消' },
          { text: '提交', onPress: value => dispatch({type:'mine/updateUserInfo',payload:{userId:user.id,nick:value?value:user.nick}}) },
        ], 'plain-text', user.nick)}
                    extra=
                      {<div
                      >{user.nick}</div>} arrow="horizontal">昵称</List.Item>
        <List.Item  thumb="https://zos.alipayobjects.com/rmsportal/dNuvNrtqUztHCwM.png" extra={user.phone?user.phone:<Link to="/bind">绑定手机</Link>} arrow="horizontal">手机</List.Item>
        <List.Item  thumb="https://zos.alipayobjects.com/rmsportal/dNuvNrtqUztHCwM.png" onClick=
                      {() => prompt('邮箱', '', [
            { text: '取消' },
          { text: '提交', onPress: value => dispatch({type:'mine/updateUserInfo',payload:{userId:user.id,email:value?value:user.email}}) },
        ], 'plain-text', user.email)}
                    extra={<div>{user.email}</div>} arrow="horizontal">邮箱</List.Item>
        <Picker extra={user.city} data={district} title="选择地区" cols={2} {...getFieldProps('district', {
          initialValue: ['anhui', 'hefei'],
        })}
        ><List.Item  thumb="https://zos.alipayobjects.com/rmsportal/dNuvNrtqUztHCwM.png"  arrow="horizontal">地区</List.Item>
        </Picker>
        <Picker extra={user.grade_year} data={grade} title="选择年级" cols={2} {...getFieldProps('grade', {
          initialValue: [1, 11],
        })}>
        <List.Item  thumb="https://zos.alipayobjects.com/rmsportal/dNuvNrtqUztHCwM.png" extra={user.grade_year} arrow="horizontal">年级</List.Item>
        </Picker>
        <List.Item  thumb="https://zos.alipayobjects.com/rmsportal/dNuvNrtqUztHCwM.png" extra="123" arrow="horizontal">收藏</List.Item>
        <List.Item  thumb="https://zos.alipayobjects.com/rmsportal/dNuvNrtqUztHCwM.png"  arrow="horizontal">学习统计</List.Item>
        <List.Item  thumb="https://zos.alipayobjects.com/rmsportal/dNuvNrtqUztHCwM.png"  arrow="horizontal">消息中心</List.Item>
        <Button type="primary" onClick={logout}>登出</Button>
      </List>
    </div>
  );
}
function mapStateToProps(state){
  const {token:userInfo,uploadToken}=state.mine;
  return{
    userInfo,
    uploadToken
  }
}
Mine=createForm()(Mine);
export default connect(mapStateToProps)(Mine);

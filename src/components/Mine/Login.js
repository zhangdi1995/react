/**
 * Created by xzsmstephen on 2017/2/20.
 */
import React from 'react';
import { List, InputItem, Switch, Stepper, Slider, Button,Modal,WhiteSpace } from 'antd-mobile';
import styles from './Mine.css'
import {connect} from 'dva'
import {browserHistory} from 'dva/router'
import {createForm} from 'rc-form'
const alert=Modal.alert;
function Login({dispatch,form,codeTime,history,location,token,login:loginStatus}) {
  var code;
  browserHistory.listen(({pathname,query})=>{
    if(query.hasOwnProperty('code')) {
      code = query.code
    }
    else{
      code=null
    }
  });


  if(loginStatus){
    dispatch({type: 'courses/fetch'});
    dispatch({type: 'explorer/fetch'});
    history.push('/mine');
  }
  else{
    if(code && !token ) {
    var wechatCode = code;
    dispatch({
      type: 'mine/loginByWechat',
      payload: {
        code: wechatCode,
        errCallback: errCallback,
        sucCallback: redirect
      }
    })
  }
  }
  console.log(!null)
  const { getFieldProps, getFieldError,getFieldValue } = form;
  console.log(getFieldProps('account', {
    // initialValue: '小蚂蚁',
    rules: [
      {required: true, message: '请输入手机号'}
    ],
  }));
  function errCallback(err) {
    alert(err.msg)
  }

  function fetchMessage() {
    dispatch({type: 'mine/fetch', payload: {userPhone: getFieldValue('account'), errCallback: errCallback}})
  }

  function login() {
    dispatch({
      type: 'mine/loginByPhone',
      payload: {
        userPhone: getFieldValue('account'),
        smsCode: getFieldValue('password'),
        errCallback: errCallback,
        sucCallback: redirect
      }
    });
    dispatch({
      type:'mine/updateCodeTime',
      payload:{
        codeTime:0
      }
    })

  }

  function redirect() {
    //todo:跳转问题,23号继续解决;解决.因为在进入login页面时判断了是否是登录状态,如果是,就跳转到/courses,这个是先执行的,所以报错.
    dispatch({type: 'courses/fetch'});
    dispatch({type: 'explorer/fetch'});
    history.push('/mine');//路径换成/courses就出问题
    alert('欢迎回来')
  }

  return (
    <form>
      <List
      >
        <InputItem
          {...getFieldProps('account', {
            // initialValue: '小蚂蚁',
            rules: [
              {required: true, message: '请输入手机号'}
            ],
          })}
          clear
          error={!!getFieldError('account')}
          onErrorClick={() => {
            alert(getFieldError('account').join('、'));
          }}
          placeholder="请输入手机号"
        >手机号</InputItem>
        <InputItem
          {...getFieldProps('password')}
          placeholder="请输入短信验证码"
          type="password"
          extra={codeTime>0?codeTime:<div onClick={fetchMessage}>获取验证码</div>}

        >
          验证码
        </InputItem>
      </List>

      <Button type="primary"
              onClick={login}
      >登录</Button>
      <WhiteSpace/>
      <a href="https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxe9c1bd75656172f1&redirect_uri=http://course.kquestions.com&response_type=code&scope=snsapi_login&pass_ticket=8eNuYvUzLPq0UB8ZFI4ta%2Fe7nRJvRNhvCVgkD7LOzSPtp3sJennqKB4WRuKS%2B2PK"><Button type="primary"

      >微信登录</Button></a>

    </form>
  )
}
function mapStateToProps(state){
  const {token,codeTime,login}=state.mine;
  return{
    token,
    codeTime,
    login

  }
}
Login=createForm()(Login);
export default connect(mapStateToProps)(Login)

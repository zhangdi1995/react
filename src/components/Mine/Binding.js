/**
 * Created by xzsmstephen on 2017/2/24.
 */
/**
 * Created by xzsmstephen on 2017/2/20.
 */
import React from 'react';
import { List, InputItem, Switch, Stepper, Slider, Button,Modal,WhiteSpace } from 'antd-mobile';
import styles from './Mine.css'
import {connect} from 'dva'
import {browserHistory} from 'dva/router'
import {createForm} from 'rc-form'
import {ERROR_INFO} from '../../constants'
const alert=Modal.alert;
function Bind({dispatch,form,codeTime,history,location,token,login:loginStatus}) {
  console.log(!null)
  let user=token.user;
  const { getFieldProps, getFieldError,getFieldValue } = form;
  console.log(getFieldProps('account', {
    // initialValue: '小蚂蚁',
    rules: [
      {required: true, message: '请输入手机号'}
    ],
  }));
  function errCallback(err) {
    alert(ERROR_INFO[err.code])
  }

  function fetchMessage() {
    dispatch({type: 'mine/fetch', payload: {userPhone: getFieldValue('account'), errCallback: errCallback}})
  }

  function binding() {
    dispatch({
      type: 'mine/bindPhone',
      payload: {
        userPhone: getFieldValue('account'),
        smsCode: getFieldValue('password'),
        userId:user.id,
        errCallback: errCallback,
        sucCallback: redirect
      }
    });
    dispatch({
      type:'mine/updateCodeTime',
      payload:{
        codeTime:0
      }
    })

  }

  function redirect() {
    //todo:跳转问题,23号继续解决;解决.因为在进入login页面时判断了是否是登录状态,如果是,就跳转到/courses,这个是先执行的,所以报错.
    history.push('/mine');//路径换成/courses就出问题
    alert('绑定成功')
  }

  return (
    <form>
      <List
      >
        <InputItem
          {...getFieldProps('account', {
            rules: [
              {required: true, message: '请输入手机号'}
            ],
          })}
          clear
          error={!!getFieldError('account')}
          onErrorClick={() => {
            alert(getFieldError('account').join('、'));
          }}
          placeholder="请输入手机号"
        >手机号</InputItem>
        <InputItem
          {...getFieldProps('password')}
          placeholder="请输入短信验证码"
          type="password"
          extra={codeTime>0?codeTime:<div onClick={fetchMessage}>获取验证码</div>}

        >
          验证码
        </InputItem>
      </List>

      <Button type="primary"
              onClick={binding}
      >绑定手机</Button>


    </form>
  )
}
function mapStateToProps(state){
  const {token,codeTime,login}=state.mine;
  return{
    token,
    codeTime,
    login

  }
}
Bind=createForm()(Bind);
export default connect(mapStateToProps)(Bind)

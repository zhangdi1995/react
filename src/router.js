import React from 'react';
import { Router, Route,IndexRedirect } from 'dva/router';
import IndexPage from './routes/IndexPage';
import Explorer from "./routes/explorer.js";
import Users from "./routes/Users.js";
import ContentList from './routes/ContentList.js'
import Content from './routes/Content.js'
import Courses from './routes/Courses'
import Lesson from './routes/Lesson'
import Mine from './routes/Mine'
import Login from './routes/Login'
import Bingding from './routes/Binding'
function RouterConfig({ history }) {
  return (
    <Router history={history}>
      <Route path="/" component={Login} />


      <Route path="/explorer" component={Explorer} />
        <Route path="/explorer/:id" component={ContentList} />
        <Route path="/contents/:id" component={Content} />
      <Route path="/courses" component={Courses} />
        <Route path="/lessons/:id" component={Lesson}/>
      <Route path="/mine" component={Mine} />
      <Route path="/bind" component={Bingding} />

    </Router>
  );
}

export default RouterConfig;

/**
 * Created by xzsmstephen on 2017/2/17.
 */
import * as coursesService from '../services/courses';
import {Toast} from 'antd-mobile'
export default {
  namespace: 'courses',
  state: {
    list: [],
    comments:[],
    lesson:[],
    currentCourse:null,
    signature:[]

  },
  reducers: {
    save(state, { payload: { data: list,comments,currentCourse,signature} }) {
      return { ...state, list,comments,currentCourse,signature};
    },
    saveLesson(state,{payload:{data:lesson}}){
      return {...state,lesson}
    },
    changeCurrentCourse(state,action){
      return {...state,currentCourse:action.payload}
    }
  },
  effects: {
    *fetch({page=1,courseId=15},{ call, put,select}) {
      var token = yield select(state=>state.mine.token);
      console.log(token);
      if(token) {
        if (token.refresh_exp < Math.round(new Date().getTime() / 1000)) {
          yield put({type:'mine/logout'})
        }
        else {
          if (token.access_exp < Math.round(new Date().getTime() / 1000)) {
            yield put({type: 'mine/updateAccessToken', payload: token});
            token = yield select(state=>state.mine.token);
          }


          const { data} = yield call(coursesService.fetch, {page,token});
          let currentCourse = data[0];
          const {data:comments} = yield call(coursesService.fetchComments, {courseId: currentCourse.id, token});

          let vid = currentCourse.video.url;
          const {data:signature} = yield call(coursesService.fetchSignature, {vid,token});
          yield put({
            type: 'save',
            payload: {
              data,
              comments,
              currentCourse,
              signature

            },
          });
        }
      }

    },
    *fetchLesson({lessonId=23},{ call, put }) {
      const { data} = yield call(coursesService.fetchLesson, { lessonId });
      console.log(data)
      yield put({
        type: 'saveLesson',
        payload: {
          data
        },
      });

    },
    *remove({ payload: id }, { call, put, select }) {
      yield call(usersService.remove, id);
      const page = yield select(state => state.users.page);
      yield put({type: 'fetch', payload: {page}});
    },
    *patch({ payload: { id, values } }, { call, put, select }) {
      yield call(usersService.patch, id, values);
      const page = yield select(state => state.users.page);
      yield put({type: 'fetch', payload: {page}});
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      dispatch({ type: 'fetch'});
      //return history.listen(({ pathname, query }) => {
      //  if (pathname === '/courses') {
      //    dispatch({ type: 'fetch'});
      //
      //  }
      //});
    },
  },
};

/**
 * Created by xzsmstephen on 2017/2/18.
 */
/**
 * Created by xzsmstephen on 2017/2/17.
 */
import * as lessonService from '../services/lesson';
import {Toast} from 'antd-mobile'
export default {
  namespace: 'lesson',
  state: {
    lesson:[],
    comments:[],
    signature:[]

  },
  reducers: {
    save(state, { payload: { data: lesson,comments,signature} }) {
      return { ...state, lesson,comments,signature};
    }
  },
  effects: {
    *fetch({payload:lessonId},{ call, put ,select}) {
      var token = yield select(state=>state.mine.token);
      if(token) {
        if (token.refresh_exp < Math.round(new Date().getTime() / 1000)) {
          yield put({type: 'mine/logout'})
        }
        else {
          if (token.access_exp < Math.round(new Date().getTime() / 1000)) {
            yield put({type: 'mine/updateAccessToken', payload: token});
            token = yield select(state=>state.mine.token);
          }
          const { data} = yield call(lessonService.fetch, {lessonId,token});
          const { data:comments} = yield call(lessonService.fetchComments, {lessonId, token});
          let vid = data.video.url;
          const {data:signature} = yield call(lessonService.fetchSignature, {vid, token});
          console.log(signature)
          yield put({
            type: 'save',
            payload: {
              data,
              comments,
              signature
            },
          });
        }
      }

    },

    *remove({ payload: id }, { call, put, select }) {
      yield call(usersService.remove, id);
      const page = yield select(state => state.users.page);
      yield put({type: 'fetch', payload: {page}});
    },
    *patch({ payload: { id, values } }, { call, put, select }) {
      yield call(usersService.patch, id, values);
      const page = yield select(state => state.users.page);
      yield put({type: 'fetch', payload: {page}});
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {

      return history.listen(({ pathname, query }) => {
        if (pathname.slice(0,8) === '/lessons') {
          dispatch({ type: 'fetch' ,payload:pathname.slice(9)});

        }
      });
    },
  },
};

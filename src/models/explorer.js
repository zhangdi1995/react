/**
 * Created by xzsmstephen on 2017/2/17.
 */
import * as explorerService from '../services/explorer';
import {Toast} from 'antd-mobile'
export default {
  namespace: 'explorer',
  state: {
    list: [],

  },
  reducers: {
    save(state, { payload: { data: list} }) {
      return { ...state, list};
    },
  },
  effects: {
    *fetch({page=1},{ call, put,select }) {
      var token = yield select(state=>state.mine.token);
      if(token) {
        if (token.refresh_exp < Math.round(new Date().getTime() / 1000)) {
          yield put({type: 'mine/logout'})
        }
        else {
          if (token.access_exp < Math.round(new Date().getTime() / 1000)) {
            yield put({type: 'mine/updateAccessToken', payload: token});
            token = yield select(state=>state.mine.token);
            console.log(token,'token2')
          }
          const { data} = yield call(explorerService.fetch, {page,token});
          console.log(data)
          yield put({
            type: 'save',
            payload: {
              data
            },
          });
        }
      }

    },
    *remove({ payload: id }, { call, put, select }) {
      yield call(usersService.remove, id);
      const page = yield select(state => state.users.page);
      yield put({type: 'fetch', payload: {page}});
    },
    *patch({ payload: { id, values } }, { call, put, select }) {
      yield call(usersService.patch, id, values);
      const page = yield select(state => state.users.page);
      yield put({type: 'fetch', payload: {page}});
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      dispatch({ type: 'fetch'});
      //return history.listen(({ pathname, query }) => {
      //  if (pathname === '/explorer') {
      //    dispatch({ type: 'fetch', payload: query });
      //  }
      //});
    },
  },
};

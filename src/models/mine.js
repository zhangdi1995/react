/**
 * Created by xzsmstephen on 2017/2/20.
 */
/**
 * Created by xzsmstephen on 2017/2/17.
 */
import * as mineService from '../services/mine';
import {Toast} from 'antd-mobile'
const delay = (timeout) => {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
};
export default {
  namespace: 'mine',
  state: {
    //每次修改token,需要跟着修改localstorage里的内容
    token:JSON.parse(localStorage.getItem('userInfo')),
    codeTime:0,
    login:localStorage.getItem('userInfo')?true:false,
    uploadToken:null

  },
  reducers: {

    save(state, { payload: { data: token} }) {
      return { ...state, token};
    },
    updateCodeTime(state,{payload:{codeTime}}){
      return {...state,codeTime}
    },
    logoutOnlyStatus(state){
      return {...state,login:false,token:null}
    },
    login(state){
      return {...state,login:true}
    },
    setUploadToken(state,{payload:{uploadToken}}){
      return {...state,uploadToken}

    }

  },
  effects: {
    *fetch({payload},{ call, put }) {

      const { data,err} = yield call(mineService.fetchMessage, payload.userPhone );
      if(data && data.code==0) {
        yield put({
          type: 'updateCodeTime',
          payload: {
            codeTime:30
          },
        });
        yield put({
          type: 'descCodeTime'
        });
      }
      else {
        err.response.json().then(error=>payload.errCallback(error))

      }


    },
    *logout({},{call,put}){

      localStorage.clear();
      yield put({type:'logoutOnlyStatus'})
    },
    *descCodeTime({},{call,put,select}){
      let codeTime= yield select((state)=>state.mine.codeTime);
      if(codeTime>0) {
        yield call(delay, 1000);
        yield put({type: 'updateCodeTime',payload:{codeTime:codeTime-1}})
        yield put({
          type: 'descCodeTime'
        });
      }
    },
    *bindPhone({payload},{call,put}){
      const { data,err} = yield call(mineService.bindPhone, {payload} );
      if(data && data.hasOwnProperty('access_token')) {

        localStorage.setItem('userInfo',JSON.stringify(data));

        yield put({type:'save',payload:{data}});
        payload.sucCallback();


      }
      else if (err) {
        err.response.json().then(error=>payload.errCallback(error))

      }
    },
    *loginByPhone({payload},{call,put}){
      const { data,err} = yield call(mineService.postLoginForm, {payload} );
      if(data && data.hasOwnProperty('access_token')) {

        localStorage.setItem('userInfo',JSON.stringify(data));

        yield put({type:'save',payload:{data}});
        yield put({type:'login'});


      }
      else if (err) {
        err.response.json().then(error=>payload.errCallback(error))

      }

    },
    *loginByWechat({payload},{call,put}){
      const {data,err} = yield call(mineService.postWechatCode,{payload});
      if(data && data.hasOwnProperty('access_token')){

        localStorage.setItem('userInfo',JSON.stringify(data));
        yield put({type:'save',payload:{data}});
        yield put({type:'login'});

      }
      else if (err){
        err.response.json().then(error=>payload.errCallback(error))
      }
    },
    *updateAccessToken({payload},{call,put}){
      const { data,err} = yield call(mineService.refreshAccessToken, {payload} );
      console.log(data)
      if(data && data.hasOwnProperty('access_token')) {
        localStorage.setItem('userInfo',JSON.stringify({...payload,...data}));
        yield put({type:'save',payload:{data:{...payload,access_exp:data.access_exp,access_token:data.access_token}}})
      }
    },
    *updateUserInfo({payload},{call,put,select}){
      let token=yield select((state)=>state.mine.token);

      const {data}=yield call(mineService.updateUserInfo,{payload,token});
      yield put({
        type:'save',
        payload:{
          data: {
            ...token,
            user: data
          }
        }
      });
      token=yield select((state)=>state.mine.token);

      localStorage.setItem('userInfo',JSON.stringify(token));
    },
    *getUserInfo({payload},{call,put,select}){
      let token=yield select((state)=>state.mine.token);

      const {data}=yield call(mineService.getUserInfo,{payload,token});
      yield put({
        type:'save',
        payload:{
          data: {
            ...token,
            user: data.user
          }
        }
      });
      token=yield select((state)=>state.mine.token);

      localStorage.setItem('userInfo',JSON.stringify(token));
    },

    *getUploadToken({payload},{call,put,select}){
      let token=yield select((state)=>state.mine.token);
      const {data}=yield call(mineService.getUploadToken,{payload,token});
      yield put({
        type:'setUploadToken',
        payload:{
          uploadToken:data

      }})
      let uploadToken=yield select((state)=>state.mine.uploadToken);
      payload.callback(uploadToken)

    },
  },
  //subscriptions: {
  //  //setup({ dispatch, history }) {
  //  //  dispatch({ type: 'fetch'});
  //    //return history.listen(({ pathname, query }) => {
  //    //  if (pathname === '/explorer') {
  //    //    dispatch({ type: 'fetch', payload: query });
  //    //  }
  //    //});
  //  },

};

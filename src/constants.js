/**
 * Created by xzsmstephen on 2017/2/17.
 */
export const PAGE_SIZE = 3;
export const district=[{label:'安徽省',value:'anhui',children:[{label:'合肥市',value:'hefei'}]},
{label:'湖北省',value:'hubei',children:[{label:'武汉市',value:'wuhan'}]}];
export const ERROR_INFO = {
  100: '没找到此用户',
  101: '需要填写手机号',
  102: '手机号无效',
  103: '需要填写验证码',
  104: '验证码无效',
  105: '此手机号已经被注册',
  110: '上传的地址不可用',
  120: '微信验证码错误',
  121: '微信验证码没有openid',
  122: '微信没有传code',
  125: '获取微信信息失败',
  126: '此微信号已经被绑定',
  130: '视频签名无效',
  150: '没有token',
  151: 'token无效',
  152: 'token过期',
  153: 'token类型不正确',
  155: 'token被禁止',
  160: '支付收据无效'
};

export const grade=[
            {
                label: '小学',
                value: 1,
                children: [
                    {
                        label: '一年级',
                        value: 11
                    }, {
                        label: '二年级',
                        value: 12
                    }, {
                        label: '三年级',
                        value: 13
                    }, {
                        label: '四年级',
                        value: 14
                    }, {
                        label: '五年级',
                        value: 15
                    }, {
                        label: '六年级',
                        value: 16
                    }
                ]
            }, {
                label: '初中',
                value: 2,
                children: [
                    {
                        label: '初一',
                        value: 21
                    }, {
                        label: '初二',
                        value: 22
                    }, {
                        label: '初三',
                        value: 23
                    }
                ]
            }, {
                label: '高中',
                value: 3,
                children: [
                    {
                        label: '高一',
                        value: 31
                    }, {
                        label: '高二',
                        value: 32
                    }, {
                        label: '高三',
                        value: 33
                    }
                ]
            }
        ]

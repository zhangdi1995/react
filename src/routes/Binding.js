/**
 * Created by xzsmstephen on 2017/2/20.
 */
import React from 'react'
import BindComponent from '../components/Mine/Binding'
import {connect} from 'dva'
import MainLayout from '../components/MainLayout/MainLayout';
function Binding({location,history}){
  return(
    <MainLayout location={location} history={history}>
      <div>
        <BindComponent location={location} history={history} />
      </div>
    </MainLayout>
  )
}
export default connect()(Binding)

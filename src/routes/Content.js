/**
 * Created by xzsmstephen on 2017/2/17.
 */
import React from 'react';
import { connect } from 'dva';
import styles from './Users.css';
import ContentListComponent from '../components/Explorer/ContentList';
import MainLayout from '../components/MainLayout/MainLayout';

function Content({location,list:dataSource,history}) {
  console.log(location)

  let contentUrl;
  dataSource.map(data=>{
    data.sections[0].public_contents.map(content=>{
      if(location.pathname.slice(10)==content.id){
        contentUrl=content.url;
      }
    })
  });

  return (
    <MainLayout location={location} history={history}>
      <div>
        <iframe width="100%"  src={contentUrl}></iframe>
      </div>
    </MainLayout>
  );
}
function mapStatetoProps(state){
  const {list}=state.explorer;
  return{
    list
  }
}
export default connect(mapStatetoProps)(Content);

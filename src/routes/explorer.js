/**
 * Created by xzsmstephen on 2017/2/17.
 */
import React from 'react';
import { connect } from 'dva';
import styles from './Users.css';
import ExplorerComponent from '../components/Explorer/Explorer';
import MainLayout from '../components/MainLayout/MainLayout';

function Explorer({location,history}) {
  return (
    <MainLayout location={location} history={history}>
      <div>
        <ExplorerComponent />
      </div>
    </MainLayout>
  );
}

export default connect()(Explorer);

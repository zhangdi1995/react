/**
 * Created by xzsmstephen on 2017/2/17.
 */
/**
 * Created by xzsmstephen on 2017/2/17.
 */
import React from 'react';
import { connect } from 'dva';
import styles from './Users.css';
import LessonComponent from '../components/Courses/Lesson';
import MainLayout from '../components/MainLayout/MainLayout';

function Lesson({location,history}) {
  return (
    <MainLayout location={location} history={history}>
      <div>
        <LessonComponent location={location} />
      </div>
    </MainLayout>
  );
}

export default connect()(Lesson);

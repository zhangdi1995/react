/**
 * Created by xzsmstephen on 2017/2/20.
 */
import React from 'react'
import {connect} from 'dva'
import MineComponent from '../components/Mine/Mine'
import MainLayout from '../components/MainLayout/MainLayout'

function Mine({history,location}){
  return(
    <MainLayout history={history} location={location}>
      <div>
        <MineComponent history={history}/>
      </div>
    </MainLayout>
  )
}
export default connect()(Mine);

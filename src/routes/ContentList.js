/**
 * Created by xzsmstephen on 2017/2/17.
 */
/**
 * Created by xzsmstephen on 2017/2/17.
 */
import React from 'react';
import { connect } from 'dva';
import styles from './Users.css';
import ContentListComponent from '../components/Explorer/ContentList';
import MainLayout from '../components/MainLayout/MainLayout';

function ContentList({location,history}) {
  return (
    <MainLayout location={location} history={history}>
      <div>
        <ContentListComponent location={location} />
      </div>
    </MainLayout>
  );
}

export default connect()(ContentList);

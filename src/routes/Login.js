/**
 * Created by xzsmstephen on 2017/2/20.
 */
import React from 'react'
import LoginComponent from '../components/Mine/Login'
import {connect} from 'dva'
import MainLayout from '../components/MainLayout/MainLayout';
function Login({location,history}){
  return(
    <MainLayout location={location} history={history}>
      <div>
        <LoginComponent location={location} history={history} />
      </div>
    </MainLayout>
  )
}
export default connect()(Login)

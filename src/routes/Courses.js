/**
 * Created by xzsmstephen on 2017/2/17.
 */
import React from 'react';
import { connect } from 'dva';
import styles from './Users.css';
import CoursesComponent from '../components/Courses/Courses';
import MainLayout from '../components/MainLayout/MainLayout';

function Courses({location,history,list}) {
  return (
    <MainLayout location={location} history={history}  >
      <div>
        <CoursesComponent />
      </div>
    </MainLayout>
  );
}

export default connect()(Courses);
